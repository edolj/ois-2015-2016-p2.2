function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf('http://sandbox.lavbic.net/teaching/OIS/gradivo/') > -1;
  if (jeSmesko) {
    sporocilo = sporocilo.replace(/\</g, '&lt;').replace(/\>/g, '&gt;').replace('&lt;img', '<img').replace('png\' /&gt;', 'png\' />');
    return $('<div style="font-weight: bold"></div>').html(sporocilo);
  } else {
    return $('<div style="font-weight: bold;"></div>').text(sporocilo);
  }
}

function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}

function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  sporocilo = filtrirajKletvice(sporocilo);
  var sistemskoSporocilo;

  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
    }
  } else {
    klepetApp.posljiSporocilo($('#kanal').text(), sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    dodajSlike(sporocilo);
    findYT(sporocilo);
  }

  $('#poslji-sporocilo').val('');
}

var socket = io.connect();

$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  var trenutniKanal = "";
  var trenutniVzdevek = "";

  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
      trenutniVzdevek = rezultat.vzdevek;
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });

  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniKanal+"@"+trenutniVzdevek);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });

  socket.on('sporocilo', function (sporocilo) {
    var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    $('#sporocila').append(novElement);
    dodajSlike(sporocilo.besedilo);
    findYT(sporocilo.besedilo);
  });

  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var kanal in kanali) {
      kanal = kanal.substring(1, kanal.length);
      if (kanal != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanal));
      }
    }

    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });

  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
    }
    
    $('#seznam-uporabnikov div').click(function() {
      var zasebniUporabnik =  $(this).text(); //event.target
      $('#poslji-sporocilo').val("/zasebno \"" + zasebniUporabnik + "\" ");
      $('#poslji-sporocilo').focus();
    });
  });

  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();

  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});

function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.replace(smesko,
      "<img src='http://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}

var vulgarne = [];
  $.get('/swearWords.csv',function(vsebina) {
  vulgarne = vsebina.split(",");
});

function filtrirajKletvice(vhod) {
  for (var i in vulgarne) {
    vhod = vhod.replace(new RegExp('\\b' + vulgarne[i] + '\\b', 'gi'), function() {
      var zamenjava = "";
      for(var j=0; j<vulgarne[i].length; j++) 
        zamenjava += "*";
      return zamenjava;  
    });
  }
  return vhod;
}

function dodajSlike(vhodnoBesedilo) {
  var reg = new RegExp('^https?:\/\/.*\.(?:png|jpg|gif)$', 'gi');
  var tabSlike = [];
  var tabela = vhodnoBesedilo.split(" ");
  for(i in tabela) {
    if(tabela[i].match(reg)) {
      tabSlike.push(tabela[i]);
    }
  }
  for(i in tabSlike) {
    $('#sporocila').append('<img src="'+tabSlike[i]+'" style="width: 200px; margin-left: 20px"/>');
  /*for(i in tabela) {
    if(tabela[i].match(reg)) {
    var slikaHtml = "<img src=\""+tabSlike[i]+"\" width=\"200\" hspace=\"20\">";
    $("#sporocila").html($("#sporocila").html()+slikaHtml);
    }
  */    
  }
  
}

function findYT(vhodnoBesedilo) {
  var tabela = vhodnoBesedilo.split(" ");
  for(i in tabela) {
    if(tabela[i].substring(0,32) === "https://www.youtube.com/watch?v=") {
      var idVideo = tabela[i].substring(32,tabela[i].length);
      $('#sporocila').append('<iframe src="https://www.youtube.com/embed/'+ idVideo+'" style="width: 150px; height: 200px; margin-left: 20px" allowfullscreen></iframe>');
    }
  }
}